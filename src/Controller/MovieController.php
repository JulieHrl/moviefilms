<?php

namespace App\Controller;

use App\Entity\Films;
use App\Entity\Recommandations;
use App\Repository\FilmsRepository;
use App\Repository\RecommandationsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class MovieController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('movie/index.html.twig', [
            'controller_name' => 'MovieController',
        ]);
    }

    /**
     * @Route("/films", name="films")
     */
    public function films(FilmsRepository $repo)
    {
        $films = $repo->findAll();

        return $this->render('movie/films.html.twig', [
            'films' => $films
        ]);
    }

    /**
     * @Route("/voirfilm/{id}", name="voirfilm")
     */
    public function voirfilm(Films $films, Request $request, RecommandationsRepository $recommandation, EntityManagerInterface $manager) {

        $recommandation = $films->getRecommandations();

        $avis = new Recommandations();

        $user = $this->getUser();

        $avis->setdate(new \DateTime())
             ->setUser($user)
             ->setFilm($films);

        $form = $this->createFormBuilder($avis)
             ->add('avis')
             ->add('note', ChoiceType::class, [
                 'choices' => [
                     '1' => 1,
                     '2' => 2,
                     '3' => 3,
                     '4' => 4,
                     '5' => 5,
                 ]
        ])
             ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($avis);
            $manager->flush();

            return $this->redirectToRoute('voirfilm', ['id' => $films->getId()]);
        }

        return $this->render('movie/voirfilm.html.twig', [
            'avisForm' => $form->createView(),
            'films' => $films,
            'avis' => $recommandation
        ]);
    }

    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('movie/about.html.twig', [
            'controller_name' => 'MovieController',
        ]);
    }







}
