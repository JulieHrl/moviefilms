<?php

namespace App\Controller;

use App\Entity\Films;
use App\Entity\Users;
use App\Form\FilmsType;
use App\Repository\FilmsRepository;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/admfilms", name="admin_films")
     * @Route("/admin/film{id}/edit", name="admin_editfilm")
     */
    public function admfilms(Films $film = null, Request $request, EntityManagerInterface $manager)
    {
        if(!$film) {
            $film = new Films();
        }

        $form = $this->createForm(FilmsType::class, $film);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($film);
            $manager->flush();

            return $this->redirectToRoute('admin_editfilm', ['id' => $film->getId()]);
        }

        return $this->render('admin/films.html.twig', [
            'formFilm' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/film{id}/delete", name="admin_deletefilm")
     */
    public function deletefilm($id, Films $film, FilmsRepository $repo, EntityManagerInterface $manager) {

        $film = $repo->find($id);

        $manager->remove($film);
        $manager->flush();

        return $this->redirectToRoute('admin_films');
    }

    /**
     * @Route("/admin/user", name="admin_users")
     */
    public function admusers(UsersRepository $repo)
    {
        $user = $repo->findAll();
        return $this->render('admin/users.html.twig', [
            'user' => $user
        ]);
    }


    /**
     * @Route("/admin/user{id}/delete", name="admin_deleteuser")
     */
    public function deleteuser($id, Users $user, UsersRepository $repo, EntityManagerInterface $manager) {

        $user = $repo->find($id);

        $manager->remove($user);
        $manager->flush();

        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/admin/user{id}/editrole", name="admin_editrole")
     */
    public function editrole(Users $user, Request $request, EntityManagerInterface $manager) {

        if($user->getRoles(["ROLE_ADMIN"])){
            $user->setRoles(["ROLE_USER"]);
        }
        else {
            $user->setRoles(["ROLE_ADMIN"]);
        }
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('admin_users');
    }
}
