<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\UsersType;
use App\Entity\Security;
use App\Form\SecurityType;
use App\Repository\SecurityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/connexion", name="security_connexion")
     */
    public function connexion()
    {
        return $this->render('security/connexion.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }

    /**
     * @Route("/inscription", name="security_inscription")
     */
    public function inscription(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder) {
        $user = new Users();

        $form = $this->createForm(UsersType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('security_connexion');
        }

        return $this->render('security/inscription.html.twig', [
            'formUser' => $form->createView()
        ]);
    }

    /**
     * @Route("/security/user{id}/edit", name="security_edituser")
     */
    public function edituser(Users $user, Request $request, EntityManagerInterface $manager) {

        $form = $this->createFormBuilder($user)
                     ->add('username')
                     ->add('nom')
                     ->add('prenom')
                     ->add('age')
                     ->getForm();

        $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {


            $manager->persist($user);
            $manager->flush();

            if ($this->getUser()->getId() == $user->getId()) {
                return $this->redirectToRoute('security_dashboard');
            }
            elseif ($this->isGranted("ROLE_ADMIN")) {
                return $this->redirectToRoute('admin_users');
            }
        }
        return $this->render('security/edituser.html.twig', [
            'formedituser' => $form->createView()
        ]);
    }

    /**
     * @Route("/security/user{id}/password", name="security_password")
     */
    public function password(Users $user, Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder) {

        $form = $this->createFormBuilder($user)
                     ->add('password', PasswordType::class)
                     ->add('confirm_password', PasswordType::class)
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('security_connexion');
        }

        return $this->render('security/password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/security/user{id}/security", name="security_security")
     */
    public function security(Users $user, Request $request, EntityManagerInterface $manager) {

        if(!$user->getSecurity()){
            $security = new Security();
        }
        else {
            $security = $user->getSecurity();
        }

        $user = $this->getUser();
        $security->setUser($user);

        $form = $this->createForm(SecurityType::class, $security);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($security);
            $manager->flush();

            return $this->redirectToRoute('security_security', ['id' => $user->getId()]);
        }

        return $this->render('security/security.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/security/lost", name="security_lost")
     */
    public function lost() {

        return $this->render('security/lost.html.twig');
    }

    /**
     * @Route("/security/check", name="security_check")
     */
    public function check(SecurityRepository $repo) {

        $mail = $_POST['email'];

        $security = $repo->findOneBy(['email' => $mail]);

        return $this->render('security/check.html.twig', [
            'security' => $security
        ]);
    }

    /**
     * @Route("/security/recover{id}", name="security_recover")
     */
    public function recover($id, Security $security, SecurityRepository $repo) {

        $security = $repo->find($id);

        $reponse = $_POST['reponse'];

        $check = $security->getReponse();

        $user = $security->getUser();

        if($reponse == $check) {

            return $this->redirectToRoute('security_password', ['id' => $user->getId()]);

        }

        return $this->render('security/connexion.html.twig', [
            'security' => $security
        ]);
    }

    /**
     * @Route("/security/dashboard", name="security_dashboard")
     */
    public function dashboard() {
        return $this->render('security/dashboard.html.twig');
    }


    /**
     * @Route("/deconnexion", name="security_deconnexion")
     */

    public function logout() {

    }
}
