<?php

namespace App\Entity;

use App\Repository\UsersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

// Mise en place des contraintes pour les formulaires
use Symfony\Component\Validator\Constraints as Assert;

// Mise en place de la vérification de l'user email afin qu'un email ne soit utilisé qu'une seule fois
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

// Implémentation de l'interface permettant l'encryptage du password
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UsersRepository::class)
 * @UniqueEntity(fields={"username"}, message="Le username est déjà utilisé")
 */
class Users implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=3, max=40)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=3, max=40)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=3, max=40)
     */
    private $prenom;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Length(min=2, max=2)
     */
    private $age;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=8, minMessage="Votre mot de passe doit faire minimum 8 caractères")
     */
    private $password;
    /**
     * @Assert\EqualTo(propertyPath="password", message="Les mots de passe doivent être identiques")
     */
    public $confirm_password;

    /**
     * @ORM\OneToMany(targetEntity=Recommandations::class, mappedBy="user")
     */
    private $recommandations;

    /**
     * @ORM\OneToOne(targetEntity=Security::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $security;

    public function __construct()
    {
        $this->recommandations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getRoles(): ?array
    {
        $roles = $this->roles;

        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    public function getSalt()
    {
    }

    /**
     * @return Collection|Recommandations[]
     */
    public function getRecommandations(): Collection
    {
        return $this->recommandations;
    }

    public function addRecommandation(Recommandations $recommandation): self
    {
        if (!$this->recommandations->contains($recommandation)) {
            $this->recommandations[] = $recommandation;
            $recommandation->setUser($this);
        }

        return $this;
    }

    public function removeRecommandation(Recommandations $recommandation): self
    {
        if ($this->recommandations->contains($recommandation)) {
            $this->recommandations->removeElement($recommandation);
            // set the owning side to null (unless already changed)
            if ($recommandation->getUser() === $this) {
                $recommandation->setUser(null);
            }
        }

        return $this;
    }

    public function getSecurity(): ?Security
    {
        return $this->security;
    }

    public function setSecurity(Security $security): self
    {
        $this->security = $security;

        // set the owning side of the relation if necessary
        if ($security->getUser() !== $this) {
            $security->setUser($this);
        }

        return $this;
    }
}
